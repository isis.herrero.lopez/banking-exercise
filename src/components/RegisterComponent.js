import React, { Component } from 'react';
import ButtonComponent from './ButtonComponent';
import {logInBubbleStyle, inputStyle, h2Style, buttonSection} from './LogInComponent';


class RegisterComponent extends Component {
    constructor(props){
        super(props);

        this.state = {
            fullName: "Full name",
            password: "Password",
            initialDeposit: "Initial cash deposit",
            buttons: [
                {name: "Back", function: this.handleClick}, 
                {name: "Register", function: this.handleClick}
            ]
        }
        this.handleCLick = this.handleClick.bind(this);
        this.getValue = this.getValue.bind(this);
    }

    getValue (e) {
        if (e.target.name === "fullName") {
            this.setState({fullName: e.target.value});
        } else if (e.target.name === "password") {
            this.setState({password: e.target.value});
        } else if (e.target.name === "initialDeposit") {
            const deposit = parseInt(e.target.value);
            this.setState({initialDeposit: deposit});
        }
    }

    handleClick = (e) => {
        const {fullName, password, initialDeposit} = this.state;
        
        const buttonClicked = e.target.name;
        if (buttonClicked ===  "Back") {
            const newValue = "true";
            this.props.changeRegisteredValue(newValue);
        } else if (buttonClicked === "Register" ){
            if (fullName === "Full name" || password === "Password" 
            || initialDeposit === "Initial cash deposit"){
                alert("Please, revise the values entered for opening your new account.");
            } else {
                if (initialDeposit < 10){
                    alert("Unfortunately we can’t open an account for such a small account.\nThe minimum deposit is 10€.");
                    this.setState({initialDeposit: "Initial cash deposit"});
                } else {
                    const newValue = {
                        fullName: fullName,
                        password: password,
                        funds: initialDeposit
                    }
                    this.props.createNewAccount(newValue);
                }
            }
        }
    }

    render(){
        const {fullName, password, initialDeposit, buttons} = this.state;
        const theseButtons = buttons.map(el =>
        <ButtonComponent key={el.name} name={el.name} onClick={el.function}>
            {el.name}
        </ButtonComponent>
        );

        return(
            <div className="bubble" style={logInBubbleStyle}>
                <h2 style={h2Style}>Register</h2>
                <input name="fullName" value={fullName} onChange={this.getValue} 
                    style={inputStyle}></input>
                <input name="password" value={password} onChange={this.getValue} 
                    style={inputStyle}></input>
                <input name="initialDeposit" value={initialDeposit} onChange={this.getValue} 
                    style={inputStyle}></input>
                <div className="buttons" style={buttonSection}>
                    {theseButtons}
                </div>
            </div>
        )
    }
}

export default RegisterComponent;