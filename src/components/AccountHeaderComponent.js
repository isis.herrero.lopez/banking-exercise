import React, {Component} from 'react';
import ButtonComponent from './ButtonComponent';

const headerStyle = {
    width: "70%",
    margin: "0 15%",
    display: "flex",
    justifyContent: "space-between",
    height: "50px"
}

const actionsStyle = {
    width: "60%",
    display: "flex",
    justifyContent: "space-between"
}

const fundsStyle = {
    withd: "30%",
    border: "1px black solid",
    backgroundColor: "white",
    marginTop: "10px",
    padding: "5px 15px",
    lineHeight: "0em"
}

class AccountHeaderComponent extends Component{
    constructor(props) {
        super(props);

        this.state = {
            actions: [
                {name: "Deposit", 
                function: this.handleClick, 
                selected: true}, 
                {name: "Withdraw", 
                function: this.handleClick, 
                selected: false}, 
                {name: "Fund requests", 
                function: this.handleClick, 
                selected: false}]
        }

        this.handleClick = this.handleClick.bind(this);
    }

    handleClick = (e) => {
        const {actions} = this.state;
        const clickedAction = actions.filter(el => el.name === e.target.name);
        const selection = clickedAction[0];
        if (selection.selected === false) {
            const unselectedAction = actions.filter (el => el.selected === true);
            const unselected = unselectedAction[0];
            const updateUnselected = {
                name: unselected.name,
                function: unselected.function,
                selected: false
            }
            const unselectedIndex = actions.findIndex (el => el.selected === true);

            const updatedSelection = {
                name: selection.name,
                function: selection.function,
                selected: true
            }
            const selectionIndex = actions.findIndex(el => el.name === e.target.name);
            this.setState(state => {
                Object.assign(actions[unselectedIndex], updateUnselected)
                Object.assign(actions[selectionIndex], updatedSelection);
                return {actions: state.actions}
            }); 
        }

        const buttonClicked = e.target.name;
        const newSubsection = buttonClicked;
        this.props.changeSubsection(newSubsection);
    }

    render() {
        const {loggedId, accounts} = this.props;
        const account = accounts.filter(el => el.id === loggedId);
        const accountInfo = account[0];

        const {actions} = this.state;
        const actionButtons = actions.map(el => 
        <ButtonComponent key={el.name} name={el.name} onClick={el.function} 
            value={el.name} selected={el.selected}
        />    
        )
        return(
            <div className="accountHeader" style={headerStyle}>
                <div className="actions" style={actionsStyle}>
                    {actionButtons}
                </div>
                <div className="fundsInfo" style={fundsStyle}>
                    <p>Total currency: {accountInfo.funds}€</p>
                </div>
            </div>
        )
    }
}
export default AccountHeaderComponent;