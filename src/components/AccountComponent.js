import React, {Component} from 'react';
import AccountHeaderComponent from './AccountHeaderComponent';
import AccountBodyComponent from './AccountBodyComponent';

const gralStyle = {
    height: "100%",
    display: "flex",
    flexDirection: "column"
}

class AccountComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            subsection: "Deposit"
        }

        this.changeSubsection = this.changeSubsection.bind(this);
    }

    changeSubsection = (newSubsection) => {
        this.setState({subsection: newSubsection});
    }

    render() {
        const {subsection} = this.state;

        return(
            <div className="accountSection" style={gralStyle}>
                <AccountHeaderComponent
                subsection={subsection}
                changeSubsection={this.changeSubsection}
                loggedId={this.props.loggedId} 
                accounts={this.props.accounts}
                />
                <AccountBodyComponent 
                subsection={subsection}
                loggedId={this.props.loggedId}
                accounts={this.props.accounts}
                updateAccount={this.props.updateAccount}
                />
            </div> 
         )
    }
}

export default AccountComponent;