import React, { Component } from 'react';
import ButtonComponent from './ButtonComponent';

const logInBubbleStyle = {
    backgroundColor: "#6fa7dc",
    border: "1px black solid",
    borderRadius: "25px",
    justifyContent: "center",
    padding: "10px",
    alingSelf: "center",
    textAlign: "center"
}

const h2Style = {
    lineHeight: "0.2em"
}

const inputStyle = {
    margin: "5px 0 7px",
    height: "2em",
    color: "grey",
    padding: "0 5px",
    fontSize: "1em"
}

const buttonSection = {
    width: "80%",
    margin: "0 10%",
    display: "flex",
    flexDirection: "row",
    justifyContent: "space-around"
}

class LogInComponent extends Component {
    constructor(props){
        super(props);

        this.state = {
            givenId: "User ID",
            givenPassword: "Password",
            buttons: [
                {name: "Register", function: this.handleClick}, 
                {name: "Enter", function: this.handleClick}
            ]
        }
        this.handleClick = this.handleClick.bind(this);
        this.getValue = this.getValue.bind(this);

    }

    getValue (e) {
        if (e.target.name === "userId") {
            const accountNumber = parseInt(e.target.value);
            this.setState({givenId: accountNumber});
        } else if (e.target.name === "password") {
            this.setState({givenPassword: e.target.value});
        }
    }

    handleClick = (e) => {
        const {givenId, givenPassword} = this.state;
        const accounts = this.props.accounts;

        const buttonClicked = e.target.name;
        if (buttonClicked === "Register") {     
            const newValue = "false";
            this.props.changeRegisteredValue(newValue);
        } else if (buttonClicked === "Enter") {
            if (givenId === "User ID" || givenPassword === "Password"){
                alert("Please, enter a valid User ID and password");
            } else {
                const accountExist = accounts.filter(el => el.id === givenId);
                if (accountExist.length === 0) {
                    alert("Sorry, but it seems that that User ID doesn't exist.\nPlease, try again or go to registration.");
                    this.setState({givenId: "User ID", givenPassword: "Password"});
                } else {
                    if (accountExist[0].password !== givenPassword){
                        alert("Sorry, but it seems that the password is not correct. Please, try again");
                        this.setState({givenPassword: "Password"});
                    } else {
                        const newValue = accountExist[0].id;
                        this.props.changeloggedId(newValue);
                    }
                }
            }
        }
    }

    render() {
        const {givenId, givenPassword, buttons} = this.state;
        const theseButtons = buttons.map(el =>
        <ButtonComponent key={el.name} name={el.name} onClick={el.function}>
            {el.name}
        </ButtonComponent>
        );

        return(
            <div className="bubble" style={logInBubbleStyle}>
                <h2 style={h2Style}>Authentication</h2>
                <input name="userId" value={givenId} onChange={this.getValue} 
                    style={inputStyle}></input>
                <input name="password" value={givenPassword} onChange={this.getValue} 
                    style={inputStyle}></input>
                <div className="buttons" style={buttonSection}>
                    {theseButtons}
                </div>
            </div>
        )
    }
}

export default LogInComponent;
export {LogInComponent, logInBubbleStyle, inputStyle, h2Style, buttonSection};