import React from 'react';
import ButtonComponent from './ButtonComponent';

const fundsHistoryStyle = {
    //later
}

const entry = {
    display: "flex",
    justifyContent: "space-between"
}

const entryInfo = {
    display: "flex",
}

const boxStyle = {
    border: "1px black solid",
    backgroundColor: "white",
    textAlign: "center",
    padding: "0px 10px",
    height: "2em",
    lineHeight: "0em",
    margin: "0.5em 10px 0 0"
}

const arrowStyle = {
    color: "#a64d79",
    fontWeight: "bolder",
    marginRight: "10px",
}

const statusStyle = {
    border: "1px black solid",
    backgroundColor: "white",
    padding: "0px 10px",
    height: "2em",
    lineHeight: "0em",
    margin: "0.5em 5px 0 0"
}
 
const FundsHistoryComponent = (props) => {
    const {accounts, loggedId, acceptButton} = props;
    console.log("fundsArea: ", accounts);
    const loggedAccount = accounts.filter(el => el.id === loggedId);
    const lendingHistory = loggedAccount[0].lendingRequests;
    const borrowingHistory = loggedAccount[0].borrowingRequests;

    const lendings = lendingHistory.map(el =>
        <div key={el.lendingId} className="entry" style={entry}>
            <div className="entryInfo" style={entryInfo}>
                <div className="box" style={boxStyle}>
                    <p>{loggedAccount[0].fullName}</p>
                </div>
                <div className="arrow">
                    <p style={arrowStyle}>&rarr;</p>
                </div>
                <div className="box" style={boxStyle}>
                    <p>{el.amount}€</p>
                </div>
                <div className="arrow">
                    <p style={arrowStyle}>&rarr;</p>
                </div>
                <div className="box" style={boxStyle}>
                    <p>To ID: {el.toId}</p>
                </div>
            </div>
            <div className="aButton" id={el.lendingId}>
                <ButtonComponent name={acceptButton.name} 
                    onClick={acceptButton.function} value={acceptButton.name} />
            </div>
        </div>
    );

    const borrowings = borrowingHistory.map(el => 
        <div key={el.borrowingId} className="entry" style={entry}>
            <div className="entryInfo" style={entryInfo}>
                <div className="box" style={boxStyle}>
                    <p>{loggedAccount[0].fullName}</p>
                </div>
                <div className="arrow">
                    <p style={arrowStyle}>&larr;</p>
                </div>
                <div className="box" style={boxStyle}>
                    <p>{el.amount}€</p>
                </div>
                <div className="arrow">
                    <p style={arrowStyle}>&larr;</p>
                </div>
                <div className="box" style={boxStyle}>
                    <p>From ID: {el.fromId}</p>
                </div>
            </div>
            <div className="status" style={statusStyle}>
                <div>
                    <p>Request pending</p>
                </div>
            </div>
        </div>
        );

    return (
        <div className="fundsHistory" style={fundsHistoryStyle}>
            <h3>Here's all your pending requests!</h3>
            {lendings}
            {borrowings}
        </div>
    )
}

export default FundsHistoryComponent;