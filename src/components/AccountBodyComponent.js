import React, { Component } from 'react';
import ButtonComponent from './ButtonComponent';
import {inputStyle} from './LogInComponent';
import FundsHistoryComponent from './FundsHistoryComponent';

const accountBodyStyle = {
    width: "64%",
    margin: "40px 18% 0"
}

const idInputStyle = {
    margin: "5px 15px 7px 10px",
    height: "2em",
    color: "grey",
    padding: "0 5px",
    fontSize: "1em",
    width: "60px"
}

class AccountBodyComponent extends Component {
    constructor(props){
        super(props);

        this.state = {
            amount: "Amount",
            toAccountId: "Fund ID",
            submitButton: {name: "Submit", function: this.handleClick},
            acceptButton: {name: "Accept", function: this.handleClick}
        }
        this.getValue = this.getValue.bind(this);
        this.handleClick = this.handleClick.bind(this);

    }

//how to empty the amount input everytime I interchage actions?
//with componentDidUpdate?

    getValue(e) {
        if (e.target.name === "amount") {
            this.setState({amount: parseInt(e.target.value)});
        } else {
            this.setState({toAccountId: parseInt(e.target.value)});
        }
    }

    handleClick = (e) => {
        const {amount, toAccountId} = this.state;
        const {accounts, loggedId} = this.props;
        const loggedAccount = accounts.filter(el => el.id === loggedId);
        const accountInfo = loggedAccount[0];
        console.log(accountInfo);

        let newInfo;
        if (e.target.name === "Submit") {
            const toDoUpdate = this.props.subsection; 
            if (toDoUpdate === "Deposit") {
                console.log("Deposit");
                const newFunds = accountInfo.funds + amount
                newInfo = {funds: newFunds};
            } else if (toDoUpdate === "Withdraw") {
                console.log("Withdraw");
                if (accountInfo.funds < amount) {
                    alert("Sorry, but you don't have enough funds in your account.\nPlease, try a smaller amount.");
                } else {
                    const newFunds = accountInfo.funds - amount;
                    newInfo = {funds: newFunds};
                }
            } else if (toDoUpdate === "Fund requests") {
                console.log("Fund requests");
                const amountToBorrow = amount;
                const borrowFromId = toAccountId;
                const borrowFromIdExist = accounts.filter(el => el.id === borrowFromId);
                if (borrowFromId === loggedId) {
                    alert("Oops! The account number provided is your own.\nPlease, try again");
                    this.setState({toAccountId: "Fund ID"});
                } else if (borrowFromIdExist.length === 1) {
                    newInfo = {
                        amountToBorrow: amountToBorrow, 
                        borrowFromId: borrowFromId
                    };
                    alert(`Great! We have registered your request and sent it to account Id ${borrowFromId}`);
                } else if (borrowFromIdExist.length === 0) {
                    alert("Oop! We don't have an account with that number. \nPlease, revise the information you provided.");
                }
            }

        } else if (e.target.name === "Accept") {
            const clickedLendingId = parseInt(e.target.parentNode.id);
            const acceptedRequest = accountInfo.lendingRequests.filter(el => el.lendingId === clickedLendingId);

            const acceptedRequestAmount = acceptedRequest[0].amount;
            if (acceptedRequestAmount > accountInfo.funds) {
                alert("Sorry, but you don't have enough funds in your account for this request.");
            } else {
                newInfo = {lendingId: clickedLendingId};
            }
        }

        if (newInfo !== undefined) {
            this.props.updateAccount(newInfo);
            this.setState({amount: "Amount", toAccountId: "Fund ID"});
        }
    }

    render() {
        const {amount, toAccountId, submitButton, acceptButton} = this.state;

        const accountInfo = this.props.accounts[0];
        let actionText;
        let extraInput;
        let fundsHistory;
        if (this.props.subsection === "Deposit"){
            actionText = "let's deposit some money into your account."
        } else if (this.props.subsection === "Withdraw") {
            actionText = "let's withdraw some money from your account."
        } else if (this.props.subsection === "Fund requests") {
            actionText = "let's plead for some funds!"
            extraInput = <input name="toAccountId" value={toAccountId} 
                onChange={this.getValue} style={idInputStyle}></input>
            fundsHistory = <FundsHistoryComponent 
            loggedId={this.props.loggedId}
            accounts={this.props.accounts}
            updateAccount={this.props.updateAccount}
            acceptButton={acceptButton}
            />;
        }

        return (
            <div className="accountBody" style={accountBodyStyle}>
                <div className="genericSection">
                    <h3>Hello, {accountInfo.fullName}, {actionText}</h3>
                    <input name="amount" value={amount} onChange={this.getValue} 
                        style={inputStyle}></input>
                    {extraInput}
                    <ButtonComponent name={submitButton.name} onClick={submitButton.function} 
                        value={submitButton.name} />
                </div>
                {fundsHistory}
            </div>
        )
    }
}

export default AccountBodyComponent;