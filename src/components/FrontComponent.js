import React from 'react';
import LogInComponent from './LogInComponent';
import RegisterComponent from './RegisterComponent';

const frontStyle = {
    display: "grid",
    gridTemplateColumns: "40% 30%",
    justifyContent: "space-around",
    padding: "40px 0"
}

const bubbleStyle = {
    backgroundColor: "#6fa7dc",
    border: "1px black solid",
    borderRadius: "25px",
    justifyContent: "center",
    padding: "10px",
    alingSelf: "center"
}

function FrontComponent(props) {
    const rightBubble = () => {
        const registered = props.registered;
        if (registered === "true") {
            return <LogInComponent 
            loggedIn = {props.loggedIn}
            changeloggedId = {props.changeloggedId}
            registered = {registered}
            changeRegisteredValue={props.changeRegisteredValue} 
            accounts={props.accounts}
            />
        } else {
            return <RegisterComponent 
            changeRegisteredValue={props.changeRegisteredValue}
            createNewAccount={props.createNewAccount}
            />
        }
    } 

    return(
    <div className="front" style={frontStyle}>
        <div className="bubble" style={bubbleStyle}>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. \
                Nulla vulputate egestas nisi vel dapibus. Vestibulum semper \
                et justo ut fermentum. Vivamus eros enim, lobortis et dolor vel,\
                 dapibus ultrices sapien. Donec magna urna, eleifend id enim nec,\
                  malesuada semper quam. Mauris ac gravida odio. \
                  Proin quis eleifend nisl. Donec nec interdum magna.</p>
        </div>
        {rightBubble()}
    </div>
    )
}

export default FrontComponent;