import React, {Component} from 'react';

const buttonStyle = {
    backgroundColor: "#ead1dc",
    color: "#a64d79",
    border: "1px black solid",
    textAlign: "center",
    padding: "5px 15px",
    margin: "10px 5px 0",
    fontSize: "1em",
    flexGrow: "1"
}

const selectedStyle = {
    color: "#ead1dc",
    backgroundColor: "#a64d79",
    border: "1px black solid",
    textAlign: "center",
    padding: "5px 15px",
    margin: "10px 5px 0",
    fontSize: "1em",
    flexGrow: "1"
}

class ButtonComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            hovered: false
        }
        this.handleHover = this.handleHover.bind(this);
    }

    handleHover(){
        this.setState({hovered: !this.state.hovered});
    }

    render() {
        const selected = this.props.selected;
        let style;
        const {hovered} = this.state;
        if (selected === true) {
            style = selectedStyle;
        } else {
            style = buttonStyle;
            
            if (hovered === false){
                style = buttonStyle;   
            } else {
                style = selectedStyle;
            }
        }

        return (
        <button name={this.props.name} onMouseEnter={this.handleHover} 
            onMouseLeave={this.handleHover} onClick={this.props.onClick} 
            style={style} value={this.props}>{this.props.name}</button>
        )
    }

}

export default ButtonComponent;