import React, { Component } from 'react';
import FrontComponent from './components/FrontComponent';
import AccountComponent from './components/AccountComponent';
import './App.css';


//styles
const headerStyle = {
  backgroundColor: "white",
  border: "1px grey solid",
  borderBottom: "0px",
  textAlign: "center",
  height: "50px" 
}

const h1 = {
  lineHeight: "0.1em"
}

const bodyStyle = {
  backgroundColor: "#cfe2f3",
  border: "1px black solid",
  borderTop: "0px",
  minHeight: "370px",
  display: "flex",
  flexDirection: "column"
}

class App extends Component {
  constructor (props) {
    super(props);

    this.state = {
      loggedIn: "false",
      registered: "true",
      loggedId: 0,
      accounts: [
        {id: 1, 
        fullName: "Mary Smith", 
        password: "hellow", 
        funds: 50, 
        lendingRequests: [
          {lendingId: 1, amount: 10, 
            toId: 2, associatedBorrowingId: 0}
        ],
        borrowingRequests: [
          {borrowingId: 0, amount: 0, fromId: 0}
        ]
        },
        {id: 2, 
        fullName: "John Smith", 
        password: "byebye", 
        funds: 100, 
        lendingRequests: [
        ],
        borrowingRequests: [
          {borrowingId: 0, amount: 10, fromId: 1}
        ]
        },
      ]
    }
    this.changeloggedId = this.changeloggedId.bind(this);
    this.changeResgisteredValue = this.changeResgisteredValue.bind(this);
    this.createNewAccount = this.createNewAccount.bind(this);
    this.updateAccount = this.updateAccount.bind(this);
    
  }

  //function to log user in and render account component
  changeloggedId = (newValue) => {
    this.setState((state, props) => ({
      loggedId: newValue,
      loggedIn: "true"
    }));
  }

  //function to render registration component
  //or to render back the logIn component
  changeResgisteredValue = (newValue) => {
    this.setState((state, props) => ({
      registered: newValue
    }));
  }

  //function to create new account
  //and render back the logIn component
  createNewAccount = (newValue) => {
    const {accounts} = this.state;
    const newId = accounts.length + 1
    const newAccount = {
      id: newId,
      fullName: newValue.fullName,
      password: newValue.password,
      funds: newValue.funds,
      lendingRequests: [],
      borrowingRequests: []
    }
    const updatedAccounts = accounts.concat(newAccount);
    this.setState({accounts: updatedAccounts});
    this.setState({registered: "true"});

    //happens before rerendering...
    alert(`Great, ${newValue.fullName}. You now have an account \
    (User ID: ${newId}) with a balance of ${newValue.funds}€.`);
  }

  //function to update account when changes happen
  updateAccount = (newInfo) => {
    const {loggedId, accounts} = this.state;
    const loggedAccount = accounts.filter(el => el.id === loggedId);
    const loggedAccountIndex = accounts.findIndex(el => el.id === loggedId);

    if (newInfo.funds) {
      const loggedAccountUpdate = {
        id: loggedAccount[0].id,
        fullName: loggedAccount[0].fullName,
        password: loggedAccount[0].password,
        funds: newInfo.funds,
        lendingRequests: loggedAccount[0].lendingRequests,
        borrowingRequests: loggedAccount[0].borrowingRequests,
      };
      this.setState(state => {
        Object.assign(accounts[loggedAccountIndex], loggedAccountUpdate);
        return {accounts: state.accounts}
      });
    } else if (newInfo.amountToBorrow) {
      //update request on both accounts
      //on the loggedAccount
      const borrowingRequestsLength = loggedAccount[0].borrowingRequests.length;
      let newBorrowingId;
      if (borrowingRequestsLength === 0) {
        newBorrowingId = 1;
      } else {
        newBorrowingId = loggedAccount[0].borrowingRequests[borrowingRequestsLength - 1].borrowingId + 1;
      }

      const newBorrowingRequest = {
        borrowingId: newBorrowingId,
        amount: newInfo.amountToBorrow, 
        fromId: newInfo.borrowFromId
      };
      const updatedBorrowingRequests = loggedAccount[0].borrowingRequests.concat(newBorrowingRequest);
      const loggedAccountUpdate = {
        id: loggedAccount[0].id,
        fullName: loggedAccount[0].fullName,
        password: loggedAccount[0].password,
        funds: loggedAccount[0].funds,
        lendingRequests: loggedAccount[0].lendingRequests,
        borrowingRequests: updatedBorrowingRequests
      };
      this.setState(state => {
        Object.assign(accounts[loggedAccountIndex], loggedAccountUpdate);
        return {accounts: state.accounts}
      });

      //om the requested account
      const fromAccount = accounts.filter(el => el.id === newInfo.borrowFromId);
      const fromAccountIndex = accounts.findIndex(el => el.id === newInfo.borrowFromId);
      const lendingRequestsLength = fromAccount[0].lendingRequests.length;
      let newLendingId;
      if (lendingRequestsLength === 0) {
        newLendingId = 1;
      } else {
        newLendingId = fromAccount[0].lendingRequests[lendingRequestsLength - 1].lendingId + 1;
      }

      const newLendingRequest = {
        lendingId: newLendingId,
        amount: newInfo.amountToBorrow,
        toId: loggedId,
        associatedBorrowingId: newBorrowingId
      }
      const updateLendingRequests = fromAccount[0].lendingRequests.concat(newLendingRequest);
      const fromAccountUpdate = {
        id: fromAccount[0].id,
        fullName: fromAccount[0].fullName,
        password: fromAccount[0].password,
        funds: fromAccount[0].funds,
        lendingRequests: updateLendingRequests,
        borrowingRequests: fromAccount[0].borrowingRequests
      };
      this.setState(state => {
        Object.assign(accounts[fromAccountIndex], fromAccountUpdate);
        return {accounts: state.accounts}
      });

    } else if (newInfo.lendingId) {
      //update both accounts too: deleting in both this request
      const thisLendingId = newInfo.lendingId;
  
      //find registry of request
      const acceptedLending = loggedAccount[0].lendingRequests.filter(el => el.lendingId = thisLendingId);

      //get info of the receiving account
      const borrowerId = acceptedLending[0].toId;
      const borrowerAccount = accounts.filter(el => el.id === borrowerId);
      const borrowerAccountIndex = accounts.findIndex(el => el.id === borrowerId);
      const borrowingId = acceptedLending[0].associatedBorrowingId;
      
      //calculate new funds for both accounts
      const updatedLenderFunds = loggedAccount[0].funds 
        - acceptedLending[0].amount;
      const updatedBorrowerFunds = borrowerAccount[0].funds 
        + acceptedLending[0].amount;

      //update the requests
      const updatedLendingRequests = loggedAccount[0].lendingRequests.filter(el => el.lendingId !== thisLendingId);
      const updatedBorrowingRequests = borrowerAccount[0].borrowingRequests.filter(el => el.borrowingId !== borrowingId);

      //update loggedAccount
      const loggedAccountUpdate = {
        id: loggedAccount[0].id,
        fullName: loggedAccount[0].fullName,
        password: loggedAccount[0].password,
        funds: updatedLenderFunds,
        lendingRequests: updatedLendingRequests,
        borrowingRequests: loggedAccount[0].borrowingRequests,
      };

      //on receving account
      const borrowerAccountUpdate = {
        id: borrowerAccount[0].id,
        fullName: borrowerAccount[0].fullName,
        password: borrowerAccount[0].password,
        funds: updatedBorrowerFunds,
        lendingRequests: borrowerAccount[0].lendingRequests,
        borrowingRequests: updatedBorrowingRequests,
      }

      //setState with updated accounts
      this.setState(state => {
        Object.assign(accounts[loggedAccountIndex], loggedAccountUpdate);
        Object.assign(accounts[borrowerAccountIndex], borrowerAccountUpdate);
        return {accounts: state.accounts}
      });
    }
  }

  render() {
    const {loggedIn, loggedId, registered, accounts} = this.state;
    console.log("App here: ", accounts)
    const pageToRender = () => {
      if (loggedIn === "false"){
        return <FrontComponent
        loggedIn = {loggedIn}
        changeloggedId = {this.changeloggedId}
        registered = {registered}
        changeRegisteredValue={this.changeResgisteredValue}
        accounts={accounts}
        createNewAccount={this.createNewAccount}
        />
      } else {
        return <AccountComponent 
        loggedId={loggedId}
        accounts={accounts}
        updateAccount={this.updateAccount}
        /> 
      }
    }
    

    return (
      <div className="App">
        <div className="header" style={headerStyle}>
          <h1 style={h1}>Buutti Banking</h1>
        </div>
        <div className="body" style={bodyStyle}>
          {pageToRender()}
        </div>
      </div>
    );
  }
}

export default App;
