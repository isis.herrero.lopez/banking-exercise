const express = require ("express");
const app = express();
const bodyParser = require("body-parser");

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

const accounts = [];

app.get("/bank", (req, res) => {
    res.sendFile(__dirname + "/index.html");
});

//create a new user
app.post("/bank/user", (req, res) => {
    let accountId;
    //supposing only one account per person is allowed
    const existingAccount = accounts.filter (el => el.name = req.body.name);
    if (existingAccount.length === 0) {
        accountId = 1;
    } else {
        const lastId = accounts[accounts.length -1].id;
        accountId = lastId + 1;
    }
    const newAccount = {
        name: req.body.name,
        accountBalance: parseInt(req.body.initialDeposit),
        password: req.body.password,
        id: accountId
    }
    accounts.push(newAccount);
    res.send(`<p>Your account Id is ${newAccount.id}.</p>`);
});

//get account balance
app.get("/bank/:user_id/balance", (req, res) => {
    const account = accounts.filter(el => el.id === parseInt(req.params.user_id));
    //I assume that the account exists and has been checked when loging in
    res.send(`<p>Your balance is ${account[0].accountBalance}€.</p>`)

});

//withdraw money
app.patch("/bank/:user_id/withdraw", (req, res) => {
    const account = accounts.filter(el => el.id === parseInt(req.params.user_id));
    if (account[0].password === req.body.password) {
        const amount = parseInt(req.body.amount);
        if (amount <= account[0].accountBalance) {
            const newBalance = account[0].accountBalance - amount;
            const updatedAccount = {...account[0], accountBalance: newBalance};
            const accountIndex = accounts.findIndex(el => el.id === parseInt(req.params.user_id));
            accounts.splice(accountIndex, 1, updatedAccount);
            const newAccountBalance = accounts[accountIndex].accountBalance;
            res.send(`<p>Your account now has a balance of ${newAccountBalance}€ </p>`);
        } else if (amount > account[0].accountBalance) {
            res.send(`<p>Oops! You don't have enough funds. Try a smaller amount.</p>`);
        }
    } else {
        res.send(`<p>The password was not correct. Please, try again.</p>`);
    }
});

//deposit money
app.patch("/bank/:user_id/deposit", (req, res) => {
    const account = accounts.filter(el => el.id === parseInt(req.params.user_id));
    if (account[0].password === req.body.password) {
        const amount = parseInt(req.body.amount);
        const newBalance = account[0].accountBalance + amount;
        const updatedAccount = {...account[0], accountBalance: newBalance};
        const accountIndex = accounts.findIndex(el => el.id === parseInt(req.params.user_id));
        accounts.splice(accountIndex, 1, updatedAccount);
        const newAccountBalance = accounts[accountIndex].accountBalance;
        res.send(`<p>Your account now has a balance of ${newAccountBalance}€.</p>`);
    } else {
        res.send(`<p>The password was not correct. Please, try again</p>`);
    }
});

//transfer money
app.patch("/bank/:user_id/transfer", (req, res) => {
    const account = accounts.filter(el => el.id === parseInt(req.params.user_id));
    if (account[0].password === req.body.password) {
        const requestedAmount = parseInt(req.body.amount);
        if (requestedAmount <= account[0].accountBalance) {
            //account giving the money
            const newBalance = account[0].accountBalance - requestedAmount;
            const updatedAccount = {...account[0], accountBalance: newBalance};
            const accountIndex = accounts.findIndex(el => el.id === parseInt(req.params.user_id));
            accounts.splice(accountIndex, 1, updatedAccount);
            const newAccountBalance = accounts[accountIndex].accountBalance;

            //account receiving the transfer
            const receivingAccount = accounts.filter(el => el.id === parseInt(req.body.recepientId));
            const receivingNewBalance = receivingAccount[0].accountBalance + requestedAmount;
            const updatedReceivingAccount = {...receivingAccount[0], accountBalance: receivingNewBalance};
            const receivingAccountIndex = accounts.findIndex(el => el.id === parseInt(req.body.recepientId));
            account.splice(receivingAccountIndex, 1, updatedReceivingAccount);

            //res.send
            res.send(`<p>Your account now has a balance of ${newAccountBalance}€ </p>`);
        } else {
            res.send(`<p>Oops! You don't have enough funds.</p>`);
        }
    } else {
        res.send(`<p>The password was not correct. Please, try again.</p>`);
    }
});

//update account 1
app.patch("/bank/:user_id/name", (req, res) => {
    const account = accounts.filter(el => el.id === parseInt(req.params.user_id));
    const newName = req.body.newName;
    const updatedAccount = {...account[0], name: newName};
    const accountIndex = accounts.findIndex(el => el.id === parseInt(req.params.user_id));
    accounts.splice(accountIndex, 1, updatedAccount);
    const newUsername = accounts[accountIndex].name;
    res.send(`<p>Your account has been updated. Now we will address you as ${newUsername}.</p>`)
});

//update account 2
app.patch("/bank/:user_id/password", (req, res) => {
    const account = accounts.filter(el => el.id === parseInt(req.params.user_id));
    const newPassword = req.body.newPassword;
    const updatedAccount = {...account[0], password: newPassword};
    const accountIndex = accounts.findIndex(el => el.id === parseInt(req.params.user_id));
    accounts.splice(accountIndex, 1, updatedAccount);
    const currentPassword = accounts[accountIndex].password;
    res.send(`<p>Your password has been changed to ${currentPassword}.</p>`)
});

console.log("Listening to port 5000");
app.listen(5000);